import { Entity, Column, Index, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Url {

    @PrimaryGeneratedColumn()
    id:number;

    @Column({
        default:null
    })
    longUrl: string;

    @Index("ind-urlId")
    @Column({
        length:100,
        default:null,
    })
    urlId:string;

}

