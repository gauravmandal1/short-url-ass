import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { UrlService } from './url.service';
import { Response } from 'express';

@Controller('url')
export class UrlController {
  constructor(private readonly urlService: UrlService) {}

  @Get('/:id')
  async findOne(@Param('id') id: string, @Res() res: Response) {
    let longUrl = await this.urlService.getLongUrl(id);
    if (longUrl!=null) {
      return res.send(`
                  <html>
                  <body>
                  <p>Redirecting to LongUrl...</p>
                  <a id="a" href="${longUrl}"></a>
                  <script>document.getElementById("a").click()</script>
                  </body>
                  </html>
                  `);
    } else {
      res.status(HttpStatus.NOT_FOUND).send('invalid params');
    }
  }

  @Get('analytics/:id')
  async getAnalyticsReport(@Param('id') id: string,@Body('startTimeStamp') startTimeStamp: number, @Body('endTimeStamp') endTimeStamp: number, @Res() res: Response) {
    let analyticReport = await this.urlService.getAnalyticsReport(id,startTimeStamp,endTimeStamp);
    if(analyticReport){
      return res.status(200).send({analyticReport});
    }
    res.status(HttpStatus.NOT_FOUND).send('invalid params');
  }

  @Post()
  async create(@Body('Url') Url: string, @Res() res: Response): Promise<{}> {
    let url = await this.urlService.create(Url);
    return res.status(HttpStatus.CREATED).send(url);
  }
}
