import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, Repository } from 'typeorm';
import { Url } from './url.entity';
import * as md5 from 'md5';
import { VisitLog } from 'src/visitLog/visitLog.entity';

@Injectable()
export class UrlService {
  constructor(
    @InjectRepository(Url)
    private urlRepository: Repository<Url>,
    @InjectRepository(VisitLog)
    private visitLogRepository: Repository<VisitLog>,
  ) {}

  async create(url: string): Promise<string> {
    let newUrl = new Url();
    newUrl.longUrl = url;
    newUrl = this.urlRepository.create(newUrl);
    await this.urlRepository.save(newUrl);
    newUrl.urlId = await this.base10ToBase36(newUrl.id);
    await this.urlRepository.save(newUrl);
    return await this.createShortUrl(newUrl.id);
  }

  async getUrl(id: number): Promise<Url> {
    return await this.urlRepository.findOne({
      where: {
        id,
      },
    });
  }

  async getLongUrl(id: string): Promise<string> {
    let params: any = id;
    params = params + '';
    if (params.length > 3) {
      let UrlId = params.substring(0, params.length - 3);
      let hash = params.substr(-3);
      if (UrlId) {
        let base10number = this.base36ToBase10(UrlId);
        let checkHash = md5(`${UrlId}${`sessionSecret`}`).slice(-3);
        if (hash == checkHash) {
          let urlObj = await this.getUrl(+base10number);
          let longUrl = urlObj?.longUrl;
          if (longUrl != null) {
            await this.addVisitLog(urlObj.urlId)
            return longUrl;
          }
        }
      }
    } else {
      return null;
    }
  }


  async getAnalyticsReport(id: string, startTimeStamp: number, endTimeStamp: number) {
    id = (id + '');
    if (id.length > 3) {
        let shortUrlId = id.substring(0, id.length - 3);
        let hash = id.substr(-3);
        if (shortUrlId) {
            let base10number = this.base36ToBase10(shortUrlId);
            let checkHash = md5(`${shortUrlId}${'sessionSecret'}`).slice(-3);
            if (hash === checkHash) { 
                let shortUrlObj = await this.getUrl(+base10number);
                return this.visitLogRepository.count({
                    where: {
                        urlId: shortUrlObj.urlId
                    }
                });
            }
        }
    }
}


  async createShortUrl(shortUrlId: number) {
    let base36Number = this.base10ToBase36(shortUrlId);
    let shortUrlHash = md5(`${base36Number}${`sessionSecret`}`).slice(-3);
    let shortUrlIdHash = `${base36Number}${shortUrlHash}`;
    return `http://localhost:3000/url/${shortUrlIdHash}`;
  }

  async addVisitLog(urlId: string) {
    let visitLog = new VisitLog()
    visitLog.urlId=urlId
    visitLog.createdStamp = +new Date()
    visitLog = this.visitLogRepository.create(visitLog);
    return this.visitLogRepository.save(visitLog);
  }

  base10ToBase36(number: number): string {
    const base36Chars: string = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (number === 0) {
      return '0';
    }
    let result: string = '';
    while (number > 0) {
      const remainder: number = number % 36;
      result = base36Chars[remainder] + result;
      number = Math.floor(number / 36);
    }
    return result;
  }

  base36ToBase10(base36Number: string): number {
    const base36Chars: string = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const reversedBase36Number: string = base36Number
      .split('')
      .reverse()
      .join('');
    let result: number = 0;
    for (let i: number = 0; i < reversedBase36Number.length; i++) {
      const char: string = reversedBase36Number[i];
      const digit: number = base36Chars.indexOf(char);
      result += digit * Math.pow(36, i);
    }
    return result;
  }
}
