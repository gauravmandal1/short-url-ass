import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UrlModule } from './url/url.module'
import { Url } from './url/url.entity';

@Module({
  imports: [TypeOrmModule.forRoot({
        type: 'mysql',
        host: "127.0.0.1",
        port: 3306,
        username: 'root',
        password: '',
        database: 'test',
        entities: [Url],
        synchronize: true,
        autoLoadEntities: true,
      }),UrlModule
    ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
