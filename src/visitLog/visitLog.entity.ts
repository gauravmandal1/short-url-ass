import { Entity, Column, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'visit_log' })
export class VisitLog {
  @PrimaryGeneratedColumn()
  id: number;

  @Index('ind-urlId')
  @Column({ length: 100, default: null })
  urlId: string;

  @Index('ind-createdStamp')
  @Column({
    type:'bigint',
    default:null,
  })
  createdStamp:number;
}