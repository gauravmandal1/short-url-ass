
# Project Title
A nest js based Url shortner with analytics.


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/gauravmandal1/short-url-ass.git
```

Go to the project directory

```bash
  cd 
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run start
```

## API Reference

#### create short Url

```http
  Post http://localhost:3000/url
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `Url` | `string` | Long url |

#### Get analytics report

```http
get http://localhost:3000/url/analytics/${urlId}

```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `startTimeStamp`      | `number` | startTime  |
| `endTimeStamp`      | `number` | endTime |



## Optimizations

I have optimized the design to enhance performance and scalability:

1)It avoids storing short URLs to eliminate disk operations, which can be a bottleneck for scalability and performance.

2)It utilizes the base36 number system in combination with the MD5 hashing algorithm to generate compact and unique short URLs efficiently.

3)The code separates the input ID into two parts: the URL ID and a hash. This separation allows for efficient validation by comparing the calculated hash with the provided hash.

4)Instead of storing long URLs directly, it stores a numerical representation (base10 number) of the URL ID. This approach can be more memory-efficient, especially when dealing with a large number of long URLs.

5)When a short URL is requested, it lazily retrieves the associated long URL from a separate data source using the getUrl function, reducing memory overhead.

These optimizations collectively aim to improve performance by minimizing disk operations, using efficient encoding and hashing techniques, implementing lazy data loading, tracking visit logs for analytics, and ensuring input validation. These measures contribute to a more scalable

